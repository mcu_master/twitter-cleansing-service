import re
import string
# nltk
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
from nltk.tokenize import word_tokenize
from  nltk.stem import SnowballStemmer

def cleaner(tweet):
    #Case Folding
    tweet = tweet.lower()
    
    # Remove Punctuation
    table = str.maketrans(dict.fromkeys(string.punctuation))  # OR {key: None for key in string.punctuation}
    tweet = tweet.translate(table)
    
    # Remove username
    tweet = re.sub("@[A-Za-z0-9]+","",tweet) #Remove @ sign
    
    # Remove Hashtag
    tweet = tweet.replace("#", "").replace("_", " ") #Remove hashtag sign but keep the text
    
    # Clean Number
    # Clean One Character
    # Remove URL
    tweet = re.sub(r"(?:\@|http?\://|https?\://|www)\S+", "", tweet) #Remove http links
    tweet = " ".join(tweet.split())
    
    # Remove RT
    retweet_key = ['rt', 'RT', 'RT@', 'rt@', '✅', '...']
    retweet_query = tweet.split()
    resultwords  = [word for word in retweet_query if word.lower() not in retweet_key]
    tweet = ' '.join(resultwords)
    L = []
    for word in tweet.split():
        if not word.endswith('…'):
            L.append(word)
           
    tweet = ' '.join(L)    

    # Convert Number
    numberPattern = r'[0-9]'
    tweet = re.sub(numberPattern, '', tweet)
    
    # Remove Stop Word
    text_tokens = word_tokenize(tweet)
    tokens_without_sw = [word for word in text_tokens if not word in stopwords.words('indonesian')]
    tweet = (" ").join(tokens_without_sw);

    # Convert Word 
    # Convert Emotion
    regrex_pattern = re.compile(pattern = "["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags = re.UNICODE)
    tweet =  regrex_pattern.sub(r'',tweet)
    
    return tweet