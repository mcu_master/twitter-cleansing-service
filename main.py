from mysql.connector.constants import ServerCmd
import pandas as pd
import mysql.connector
import config.db_mysql as dbconf
import bin.preproc as preproc
import re
import schedule
import time


mydb = mysql.connector.connect(
  host=dbconf.host,
  user=dbconf.user,
  password=dbconf.password,
  database=dbconf.database
)

def clean():
  mycursor = mydb.cursor()
  
  try:
    mycursor.execute("SELECT id, tweet_text FROM tweets where tweet_text_clean is null  ORDER BY ID ASC LIMIT 1,10")
    myresult = mycursor.fetchall()
  except Error as e:
    print("Error Connected to Database")


  for x in myresult:
    clean_tweet = preproc.cleaner(x[1])
    mycursor = mydb.cursor()
    sql = "UPDATE tweets SET tweet_text_clean = %s WHERE id=%s"
    print(sql)
    mycursor.execute(sql, (clean_tweet,x[0]))
    id = x[0]
    mydb.commit()

def location_clean():
  print('Running Clean Location...')
  mycursor = mydb.cursor()
  
  #Delete - ? ??
  try:
    mycursor.execute("update from tweets set places = NULL where places in ('-', '?', '??') ")
  except Error as e:
    print("Error Update")

def location_clean2():
  print('Running Clean Location 2')
  mycursor = mydb.cursor()
  
  try:
    mycursor.execute("SELECT places FROM tweets WHERE places IS NOT NULL LIMIT 1,100")
    myresult = mycursor.fetchall()
  except Error as e:
    print("Error getting place location")


  for x in myresult:
    if(len(x[1])<=2):
      sql = "UPDATE tweets SET places = NULL WHERE id=%s"
      mycursor.execute(sql, (x[0]))
    else:
      clean_location = x[1].replace("?", "").replace("_", " ");
      sql = "UPDATE tweets SET places = %s WHERE id=%s"
      mycursor.execute(sql, (clean_location, x[0]))
    mydb.commit()
    
schedule.every(30).seconds.do(clean)
schedule.every(2).seconds.do(location_clean)
schedule.every(2).seconds.do(location_clean)

while 1:
    schedule.run_pending()
    time.sleep(1)